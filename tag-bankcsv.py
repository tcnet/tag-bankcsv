import argparse
import os
import sys
import time
import re
from shutil import copyfile

import pandas as pd
from colorama import Fore, Style
from openpyxl import load_workbook
from pandas import ExcelWriter

from errno import EACCES, EPERM, ENOENT


'''
This script will open up a bank statements
at the given file path, add a pre-defined tag header
and insert a category based on the transaction.

A new file will be written to the designated file location.
'''

'''
TODO
add amazon tranactions
add ebay transactions
    check if ebay transaction is positive or negative. This will determine if income or online transaction
'''


'''
opens each .csv in a given system path
adds a "Tag" column 
converts Date column to datetime object
converts Tag column to string object
saves to abspath
'''
def normalize_csv(df, abspath):

    # insert a new Tag column
    if 'Tag' not in df.columns:
        print ("Inserting Tag column")
        df.insert(len(df.columns), 'Tag', '')
        df['Tag'] = df['Tag'].astype('str') # converts Tag column to string object
    
    # rename Transaction Date to Date
    if 'Transaction Date' in df.columns:
        df.rename({'Transaction Date': 'Date'}, axis=1, inplace=True)
    
    # drop Posted Date column
    if 'Posted Date' in df.columns:
        df.drop(['Posted Date'], axis=1, inplace=True)

    # drop Category column
    if 'Category' in df.columns:
        df.drop(['Category'], axis=1, inplace=True)
    
    # rename debit to charges
    if 'Debit' in df.columns:
        df.rename({'Debit': 'Charges'}, axis=1, inplace=True)

    # rename credit to Payments
    if 'Credit' in df.columns:
        df.rename({'Credit': 'Payments'}, axis=1, inplace=True)

    # rename Name to Description
    if 'Name' in df.columns:
        df.rename({'Name': 'Description'}, axis=1, inplace=True)

    # rename Amount to Charges
    if 'Amount' in df.columns:
        df.rename({'Amount': 'Charges'}, axis=1, inplace=True)

    # converts Date column to datetime object
    if 'Date' in df.columns:
        df['Date'] = pd.to_datetime(df['Date'], infer_datetime_format=True, errors='ignore')
    
    '''
    removes consecutive spaces and apostrophes from description strings
    '''
    # iterates over dataframe rows
    for idx, row in df.iterrows():
        descString = df.at[idx, 'Description'] # assign string in current description row
        if check_multiple_white_spaces(descString): # checks if string in Description column has consecutive spaces
            noSpaces = " ".join(descString.split()) # removes consecutive spaces in string and assigns to new variable
            df.at[idx, 'Description'] = noSpaces

        descString = df.at[idx, 'Description'] # assign string in current description row

        if check_unwanted_spec_chars(descString): # checks if any unwanted special characters exist in string
            normalizedString = re.sub(r"[\'\`]+", "", descString) # substitutes any unwated characters with nothing
            df.at[idx, 'Description'] = normalizedString # writes new string to Description cell

    # write dataframe to file. index=False avoids index being written to data frame
    try:
        df.to_csv(abspath, index=False)
    except (IOError, OSError) as e:
        print_error_message(e,abspath)
        sys.exit()
    except:
        print('Unexpected error:', sys.exc_info()[0])
        sys.exit()


'''
returns true if a given string has 2 or more consecutive spaces
'''
def check_multiple_white_spaces(strToCheck):
    return bool(re.search(r' {2,}', strToCheck))


'''
returns true if a given string has 1 or more apostrophes
'''
def check_unwanted_spec_chars(strToCheck):
    return bool(re.search(r'[\'\`]+', strToCheck))


# check the Tag header in .csv files for "None"
def check_none_tag(fpath):
    
    none_checking_tag = []
    none_credit_tag = []
    none_paypal_tag = []

    for fname in os.listdir(fpath):
        abspath = (os.path.join(fpath, fname))

        # CHECKING
        if fname.lower().endswith('.csv') and "checking" in fname.lower():
            df = pd.read_csv(abspath, engine='python')

            for idx,row in df.iterrows():  # read every key in dataframe
                if "None" in df["Tag"] or df["Tag"].isnull().iloc[idx]:
                    # append the row's description to a list
                    none_checking_tag.append(row["Description"])

        # CREDIT
        elif fname.lower().endswith('.csv') and "credit" in fname.lower():
            df = pd.read_csv(abspath, engine='python')
        
            for idx,row in df.iterrows(): # read every key in dataframe
                if "None" in df["Tag"] or df["Tag"].isnull().iloc[idx]:
                    # append the row's description to a list
                    none_credit_tag.append(row["Description"])
        
        # PAYPAL
        elif fname.lower().endswith('.csv') and "paypal" in fname.lower():
            df = pd.read_csv(abspath, engine='python')
        
            for idx,row in df.iterrows(): # read every key in dataframe
                if "None" in df["Tag"] or df["Tag"].isnull().iloc[idx]:
                    # append the row's description to a list
                    none_paypal_tag.append(row["Type"])

    if len(none_checking_tag) > 0:
        print(f'\n{Fore.RED}Checking Statement items that need to be categorized: {Style.RESET_ALL}')
        print(none_checking_tag)
    
    if len(none_credit_tag) > 0:
        print(f'\n{Fore.BLUE}Credit Statement items that need to be categorized: {Style.RESET_ALL}')
        print('{}\n'.format(none_credit_tag))
    
    if len(none_paypal_tag) > 0:
        print(f'\n{Fore.GREEN}Paypal Statement items that need to be categorized: {Style.RESET_ALL}')
        print('{}\n'.format(none_paypal_tag))


# checks master csv file for duplicate rows, deletes them and sorts them
def check_for_duplicates(fpath):
    
    for fname in os.listdir(fpath):
        abspath = (os.path.join(fpath, fname))

        if fname.lower().endswith('.csv') and "all_transactions" in fname.lower():
            df = pd.read_csv(abspath, engine='python')
            # sort dataframe values by date in ascending order
            df = df.sort_values(by=['Date'], ascending=False)
            # removes duplicate rows in csv
            df = df.drop_duplicates()
            df.to_csv(abspath, index=False)


# condenses all csv files into a master file
def create_master_csv(fpath):
    
    # set flag = 0 to avoid header being written every time a new .csv file is read
    flag = 0
    
    # removes master csv file if it exists
    remove_master_csv(fpath)
    
    for fname in os.listdir(fpath):
        abspath = (os.path.join(fpath, fname))

        # CHECKING
        if fname.lower().endswith('.csv') and "checking" in fname.lower():
            # create abspath for master csv file
            master_csv_path = os.path.join(fpath, 'all_transactions_ck.csv')
            df = pd.read_csv(abspath, engine='python')[['Date', 'Description', 'Charges', 'Payments', 'Tag']]
            # only write header to file the first time
            if flag == 0:
                print ("\nWriting master csv: {}\n".format(master_csv_path))
                df.to_csv(master_csv_path, index=False)
                flag = 1
            elif flag == 1:
                # set to append mode
                df.to_csv(master_csv_path, mode='a', index=False, header=False)

        # CREDIT
        elif fname.lower().endswith('.csv') and "credit" in fname.lower():
            # create abspath for master csv file
            master_csv_path = os.path.join(fpath, 'all_transactions_cc.csv')
            df = pd.read_csv(abspath, engine='python')[['Date', 'Description', 'Charges', 'Payments', 'Tag']]
            # only write header to file the first time
            if flag == 0:
                print ("\nWriting master csv: {}\n".format(master_csv_path))
                df.to_csv(master_csv_path, index=False)
                flag = 1
            elif flag == 1:
                # set to append mode
                df.to_csv(master_csv_path, mode='a', index=False, header=False)

        # PAYPAL
        elif fname.lower().endswith('.csv') and "paypal" in fname.lower():
            # create abspath for master csv file
            master_csv_path = os.path.join(fpath, 'all_transactions_pp.csv')
            df = pd.read_csv(abspath, engine='python')[['Date', 'Description', 'Type', 'Charges', 'Tag']]
            # only write header to file the first time
            if flag == 0:
                print ("\nWriting master csv: {}\n".format(master_csv_path))
                df.to_csv(master_csv_path, index=False)
                flag = 1
            elif flag == 1:
                # set to append mode
                df.to_csv(master_csv_path, mode='a', index=False, header=False)


# reads master .csv file and stores it in budget .xlsx file
def master_csv_to_excel_sheet(fpath, checking_cols, credit_cols, paypal_cols):
    
    xlsx_fpath = '/media/pluto/docs/finance/budget/test-budget.xlsx'

    # checks for a keyword in the given filepath
    # assigns sheet_name variable
    # reads master csv and assigns to df_csv
    # reads budget excel sheet and assigns to df_xlsx
    if "checking" in fpath.lower():
        master_csv_path = os.path.join(fpath, 'all_transactions_ck.csv')
        sheet_name = 'Checking_Expenses'
        df_csv = pd.read_csv(master_csv_path, engine='python')
    elif "credit" in fpath.lower():
        master_csv_path = os.path.join(fpath, 'all_transactions_cc.csv')
        sheet_name = 'CC_Expenses'
        df_csv = pd.read_csv(master_csv_path, engine='python')

    elif "paypal" in fpath.lower():
        master_csv_path = os.path.join(fpath, 'all_transactions_pp.csv')
        sheet_name = 'Paypal'
        df_csv = pd.read_csv(master_csv_path, engine='python')

    try:
        writer = pd.ExcelWriter(xlsx_fpath, date_format='MM-DD-YYYY', datetime_format='MM-DD-YYYY', mode='a', engine='openpyxl')

        writer.book = load_workbook(xlsx_fpath)

        # get the last row in the existing Excel sheet
        # if it was not specified explicitly
        #if startrow is None and sheet_name in writer.book.sheetnames:
        #    startrow = writer.book[sheet_name].max_row

        # truncate sheet
        if sheet_name in writer.book.sheetnames:
            # index of [sheet_name] sheet
            idx = writer.book.sheetnames.index(sheet_name)
            # remove [sheet_name]
            writer.book.remove(writer.book.worksheets[idx])
            # create an empty sheet [sheet_name] using old index
            writer.book.create_sheet(sheet_name, idx)
                
        # assign existing sheets to a dictionary
        writer.sheets = {ws.title: ws for ws in writer.book.worksheets}
    except FileNotFoundError:
        # file does not exist yet, we will create it
        pass

    # write dataframe to the new sheet
    df_csv.to_excel(writer, sheet_name, startrow=0, index=False)

    # save the workbook
    writer.save()


# prints error messages pertaining to files
def print_error_message(e, file_name):
    #PermissionError
    if e.errno==EPERM or e.errno==EACCES:
        print("PermissionError error({0}): {1} for:\n{2}".format(e.errno, e.strerror, file_name))
    #FileNotFoundError
    elif e.errno==ENOENT:
        print("FileNotFoundError error({0}): {1} as:\n{2}".format(e.errno, e.strerror, file_name))
    elif IOError:
        print("I/O error({0}): {1} as:\n{2}".format(e.errno, e.strerror, file_name))
    elif OSError:
        print("OS error({0}): {1} as:\n{2}".format(e.errno, e.strerror, file_name))


# 1. adds a header tag if it doesn't exist
# 2. checks description columns and adds a category for each transactions based on the dictionaries
def read_csv_add_tags(fpath, transactions, pp_transactions):
    
    for fname in os.listdir(fpath):
        abspath = (os.path.join(fpath, fname))        
        
        # CHECKING
        if fname.lower().endswith('.csv') and "checking" in fname.lower():
            # open csv file and create dataframe object  
            df = pd.read_csv(abspath, engine='python')
            normalize_csv(df, abspath)
            # read every key in transactions dictionary
            for keys in transactions:
                # read every value in transactions dictionary
                for value in transactions[str(keys)]:
                    # iterates over dataframe rows
                    for idx, row in df.iterrows():
                        # if dictionary value is found in Description column
                        if value in df.at[idx, 'Description']:
                            # write transaction categories to dataframe in the "Tag" column
                            df.at[idx, 'Tag'] = keys

            writecsv(abspath, df)

        # CREDIT
        elif fname.lower().endswith('.csv') and "credit" in fname.lower():
            df = pd.read_csv(abspath, engine='python')
            normalize_csv(df, abspath)
            for keys in transactions:  # read every key in transactions dictionary
                for value in transactions[str(keys)]: # read every value in transactions dictionary
                    for idx, row in df.iterrows():  # iterates over dataframe rows
                        if value in df.at[idx, 'Description']: # if dictionary value is found in Description column. row index/column
                            df.at[idx, 'Tag'] = keys  # write transaction categories to dataframe in the "Tag" column

            writecsv(abspath, df)

        # PAYPAL
        elif fname.lower().endswith('.csv') and "paypal" in fname.lower():
            df = pd.read_csv(abspath, engine='python')
            normalize_csv(df, abspath)
            for keys in pp_transactions:  # read every key in pp_transactions dictionary
                for value in pp_transactions[str(keys)]: # read every value in pp_transactions dictionary
                    for idx, row in df.iterrows():  # iterate over dataframe rows
                        if value in df.at[idx, 'Type']:  # if dictionary value is found in 'Type' column. row index/column
                            df.at[idx, 'Tag'] = keys # write transaction categories to dataframe in the "Tag" column

            writecsv(abspath, df)


def remove_master_csv(fpath):
    master_checking_csv = os.path.join(fpath, 'all_transactions_ck.csv')
    master_credit_csv = os.path.join(fpath, 'all_transactions_cc.csv')
    master_paypal_csv = os.path.join(fpath, 'all_transactions_pp.csv')
    
    # if file exists, delete it
    if os.path.isfile(master_checking_csv):
        try:
            os.remove(master_checking_csv)
        except (FileNotFoundError, IOError):
            print("File doesn't exist. Exiting...")
            exit()
    elif os.path.isfile(master_credit_csv): 
        try:
            os.remove(master_credit_csv)
        except (FileNotFoundError, IOError):
            print("File doesn't exist. Exiting...")
            exit()
    elif os.path.isfile(master_paypal_csv):
        try:
            os.remove(master_paypal_csv)
        except (FileNotFoundError, IOError):
            print("File doesn't exist. Exiting...")
            exit()


# input from read_csv_add_tags() function includes:
# filename, absolute path and dataframe
def writecsv(abspath, df):

    # sort df values by date in ascending order
    df = df.sort_values(by=['Date'], ascending=False)
    # check for duplicate rows and delete them
    df = df.drop_duplicates()

    # tries to write dataframe to output file if found.
    # if not found, exit the program.
    if "checking" in abspath.lower():
        try:
            print ("Writing file: {}".format(abspath))
            # write data frame to abspath (abspath, index=False)
            df.to_csv(abspath, index=False)
        except (FileNotFoundError, IOError):
            print("File path doesn't exist. Exiting...")
            exit()

    elif "credit" in abspath.lower():
        try:
            print ("Writing file to: {}".format(abspath))
            # write data frame to output file
            df.to_csv(abspath, index=False) 
        except (FileNotFoundError, IOError):
            print("File path doesn't exist. Exiting...")
            exit()

    elif "paypal" in abspath.lower():
        try:
            print ("Writing file to: {}".format(abspath))
            # write data frame to output file
            df.to_csv(abspath, index=False)
        except (FileNotFoundError, IOError):
            print("File path doesn't exist. Exiting...")
            exit()

    else:
        print("Credit, Checking or Paypal not found in file name. Exiting...")
        exit()


def main():
    #checking_cols = ['Date', 'Check Number', 'Description', 'Debit', 'Credit', 'Running Balance', 'Tag']
    #credit_cols = ['Date', 'Description', 'Card No.', 'Charges', 'Payments', 'Tag']
    #paypal_cols = ['Date', 'Time', 'TimeZone', 'Name', 'Type', 'Status', 'Currency', 'Amount', 'Receipt', 'ID', 'Balance', 'Tag']

    # dictionary of transactions. Each key is a transaction catagory,
    # and the values are where money was spent
    transactions = {
        'Income': ['PAYROLL', 'MOBILE CHECK DEPOSIT', 'BOARD OF TRU', 'SUNTRUST REWARDS',
            'UNIVERSITY OF CE DIRECT PAY'],
        'Income - Amazon': ['AMZNXDR6Q1MY     MARKETPLAC'],
        'Income - Misc': ['FROM E LAWRENCE','FROM G HUGUENS','CASHREWARDS',
            'SUNTRUST EXT TFR ZELLE EMANUEL GONZALE', 'OFFER REWARD PART 1','BTC 8003435845 4R8ZNY44E96A',
            'BTC 8003435845 62STVX0SE96A','BTC 8003435845 8VU4S6GGE96A','BTC 8889087930', 'PAYPAL TRANSFER',
            'FROM CXC','CREDIT-TRAVEL REWARD','APPLE CASH TRANSFER'],
        'Income - Reimbursement': ['VENMO CASHOUT','ATM DEPOSIT','INCOMING FEDWIRE CR TRN #012556'],
        'Income - Rent': ['FROM E GONZALEZ'],
        'Income - Taxes': ['IRS TREAS','TAX REF'],
        
        # transfers from savings. this should negate the initla transfer to savings
        'Income - Savings': ['ONLINE BANKING TRANSFER FROM 0175 00','ALLY BANK P2P',
            'ALLY BANK        P2P'],
        
        # transfers to savings
        'Savings': ['ALLY BANK $TRANSFER','ATM CASH WITHDRAWAL','ONLINE BANKING TRANSFER TO 0175 00'],
        
        # oil, repairs, and other maintenance for autos
        'Automotive - Repair/Maintenance': ['ADVANCE AUTO PARTS','ALL TRANSMISSION WORLD','AUTOZONE',
            'FIRESTONE'],
        'Automotive - Car Charging': ['TESLA 1888-518-3752CA','SEMACONNECT'],
        'Gas': ['CHEVRON','EXXONMOBIL','KANGAROO EXPRESS','CITGO','7-ELEVEN','SPEEDWAY','SHELL SERVICE',
            'WAWA','CIRCLE K','SUNOCO','SHELL OIL','RACETRAC'],
        
        # Water, electricity, garbage, and other basic utilities expenses
        'Bill - Car Insurance': ['PROGRESSIVE', 'GEICO','SAFECO INSURANCE'],
        'Bill - House Insurance': ['GULFSTREAM','AGI*RENTERS'],
        'Bill - Car Payment': ['HARRIS NA'],
        'Bill - Electricity': ['DUKEENERGY','DUKE-ENERGY', 'DUKE ENERGY'],
        'Bill - Internet': ['BRIGHT HOUSE','SPECTRUM'],
        'Bill - Mortgage': ['AMERIHOME','OUTGOING FEDWIRE DR TRN'],
        'Bill - Phone': ['AT&T'],
        'Bill - Rent': ['BORROW RENT','SRP SUB LLC'],
        'Bill - Water': ['ORANGE COUNTY UTILITIE'],
        
        # Bank account service fees, interest fees, bad check charges and other bank fees
        'Bank Charges/Fees': ['TRANSFER FEE','FEE $03.00','OVERDRAFT ITEM FEE',
            'L2G*SERVICE FEE','Interest Charge','INTEREST CHARGE','CASH ADVANCE FEE'],

        # CREDIT CARD PAYMENTS        
        'Credit - Amazon': ['PAYMENT FOR AMZ','AMZ STORECRD PMT'],
        'Credit - Addition': ['VISA PLAT PMT','CFEFCU','CHECK','ADDITIONCU CR CD ONLINE PMT CKF'],
        'Credit - Capital One': ['CAPITAL ONE ONLINE PMT'],
        'Credit - Chase': ['CHASE CREDIT','CHASE EPAY'],
        'Credit - Paypal': ['PAYPAL ECHECK','PAYPAL INST XFER 100'],
        'Credit - Plumbing Loan': ['SYNCHRONY BANK'],
        'Credit - Student Loans': ['NELNET', 'DEPT EDUCATION'],
        
        # ONLINE BANKING TRANSFER TO 0175 - The payment from checking to cc
        # PAYMENT - THANK YOU ATLANTA GA - the credit that shows on the cc statement
        'Credit - Suntrust': ['MOBILE APP PAYMENT TO 0175','ONLINE BANKING TRANSFER TO 0175 55',
            'ONLINE BANKING TRANSFER TO 0175'],
        'Credit - Suntrust Loan': ['LIGHTSTREAM'],
        'Credit - Target': ['CARD SRVC BILL PAY'],


        'Food - Dining Out': ['ARAMARK','ALE HOUSE','DINING','BAGEL','BAKERY','BAR','BEER','BENTO',
            'BISCUIT','BUFFALO WILD WINGS','BRASS TAP','BURGER','CAFE','CADDYSHANKS','CANDY','CHEDDAR',
            'CHEESECAKE','CHICK','CHILI','CHIPOTLE','CLOAK BLASTER','COFFEE','COTTONEXCHANGE',
            'CRISPERS','DAILYPOUTINE','DAVE & BUSTERS','DELI','DINER','DOMINO','FIDDLERS GREEN','FIRST WATCH',
            'GERMANY PRETZEL','GRINGO','HERO HOT DOG','HOPPING POT','HOUSE OF BLUES','WEASLEYS',
            'HUEYMAGOOS','IHOP','ITALIAN ICE','IZZIBAN','JACK OF THE WOOD','JIMMY JOHNS','LAZY MOON',
            'LIGHTHOUSECOVE','LONGHORN','MARGARITA','MARKET','MENCHIES','MERRITT','MOE','CUBSCOUTS',
            'MOES','MOUNTAIN LAYER','MURDOCKS','MUZZARELLA','NOLANS','OLIVEGARDEN','OMELET',
            'PANDA EXPRESS','PANERA BREAD','PANTRY','PEIWEI','PHILLY', 'STEAK','PIZZA','PLANET HOLLYWOOD',
            'POLLO TROPICAL','PUBLICHOUSE','QDOBA','RUBYTUESDA','FOOD','SHAKE SHACK','SMOKE HOUSE',
            'SMOOTHIE','SPRINKLES','STEAK N SHAKE','SWEET BY GOOD','SWEET PICK N MIX','SWEET TREASURE',
            'TACO','TAPHOUSE','TAPROOM','TAVERN','TEA','THE HOPPING PO','THUNDERFALLS',
            'TIJUANA FLATS','TUTTI FRUTTI','UNIV-OF-C','UNIVERSITY OF CEN','VALKYRIE DOUGH',
            'VONSTEPHAN VILLAGE','WALL ST PLAZA','WHICH WICH','WINE','WOB','WOLFGANG PUCK','YOURPIE',
            'ZAPATA','THE TOOTHSOME','LECHONERA','DUFF GARDENS','BONE CHILLERS','MRS FIELD','MELS DRIVE-IN',
            'SOCIAL HOUSE','TAPINGO','SIMPSON S','BACKLOT EXPRESS','SUNSET RANCH','ST TIP BOARD COMBO',
            'RAGLAN ROAD','EPCOT','BOWL SPOT','COWFISH','EINSTEINS','SUGAR SHACK','HONEYDUKES',
            'MCALISTERS','4RIVERS','ROSIE\'S ALL-AMERICA','JEREMIAH\'S','BISTRO','TREATS','THE POP',
            'FAMOUS FAMIGLIA','UCF HUEY MAGOOS','WATERFORD LAKES 20','RISING TIDE TAP','PUB AMERICANA','FIVE GUYS',
            'STARBUCKS','THE NOOK', 'COPPER KETTLE', 'JERSEY MIKES', 'HUMMUS HOUSE', 'SPICE INDIAN GRILL', 'APPLEBEES',
            'KELLY\'S HOMEMADE ICE', 'SUSHI', 'GREEN BENCH BREWING', 'SE7ENBITES', 'SMOKEHOUSE', 'PROCCOLINOS',
            'CHUY S','SHAMROCK BEVERAGE','DAILY POUTINE','WEST SIDE LANDING','ST GRAND AVE','MEIS KITCHEN'],
        'Food - Groceries': ['PUBLIX', 'INSTACART', 'TRADER JOE', 'BULK NATION','WM SUPERCENTER','TARGET', 'FRESCO Y MAS',
            'ALDI', 'WINN-DIXIE', 'ANAHEIM PRODUCE','ST AUGUSTINE DISTILLER','WALGREENS','DINOSTORE'],
        'Crypto': ['COINBASE','GENESISMINING','LEDGER'],
        'Donation': ['WUCF'],
        'Doggo': ['EAST ORLANDO ANIMAL', 'PETSMART', 'PETLAND','PET PAL ANIMAL'],
        
        # movies, video games, electronics, books, etc
        'Entertainment': ['REGAL CINEMAS', 'REG WATERFORD','ROCK CLIMBING', 'COMICS', 'GAMES','BEST BUY', 'SOCCER', 
            'ITUNES', 'THE GAME', 'STAR WARS','AMC ALTAMONTE', 'NINTENDO', 'BOARDWALK', 'ORLANDO KART',
            'LEGACY GOLF & TENNIS','COLISEUM OF CO','MOVIEPASS','EPIC AXE THROW','LIGHTHOUSE COVE'],
        
        # Tickets for theme parks, concerts, etc
        'Entertainment - Tickets/Events': ['KENNEDY SPACE CENTER','DISNEY','UNIVERSAL','TICKET',
            'GAYLORD PALMS','CENTERPLATE','FANATICS SPECIAL EVENT','SLING.COM','THE PLAZA LIVE'],
        'Haircut': ['HAIR CUTTERY','SPORT CLIPS','HAIR STUDIO','MASTERCUTS', 'FLOYDS 99'],
        
        # tools, building supplies, appliances
        'Home Improvement': ['HARBOR FREIGHT','HOME DEPOT','HOMEDEPOT.COM','LOWES','LOWE\'S','ACE HARDWAR',
            'IKEA','BADCOCK','IMPORTS','WOOD','ACMETOOLS','*KATZMOSESWO','*HOMDEPOT'],
        
        # transactions that should be ignored. Use this category when a debit and a credit shows up on different statements.
        # example: you pay 100 to a credit card from checking account, that shows on cc statement as a 'credit'
        'Ignore': ['PAYMENT - THANK YOU ATLANTA GA', 'CAPITAL ONE ONLINE PYMT'],
        
        # costs related to mdair
        'MD Air': ['JOHNSTONE SUPPLY', 'GOOGLE', 'TO M CAMPBELL', 'SITEGROUND', 'VISTAPRINT',
            'OMNI HOUSTON','UA INFLT','THUMBTACK','TO J FAGUNDES'],
        
        # costs related to tbc-essentials
        'TBC-Essentials': ['ALIBABA', 'HONEST HIPPO', 'US PATENT TRADEMARK', 'MONEYGRAM', 'HELIUM 10', 'Amazon seller repay',
            'TIPTRANS','PAYPAL *SPEEDYBARCO'],
        
        # transactions without a proper category or payments to friends/relatives
        'Misc': ['VENMO PAYMENT','ISC 2','ISC2','CITY OF ORLANDO', 'GIFTS', 'TO E GONZALEZ',
            'TO D FAGUNDES', 'LEGO','PARKING SERVICES','CUSTOMINK','MAGIC SHOP','GLOBAL VEND','CENTERPOINTE',
            'SURPLUS','MINUTE KEY','BRIAN BETHUNE','LEORY DE LEON','INFORMA POP CULTURE','SUNTRUST EXT TFR',
            'XSOLLA','XFER  HER','XFER  100', 'ESTATE SALE','THGUSA','CASH APP*HUGUENS','TO E DELGADO'],
        
        # clothes, shoes, house decor, merchandise, online shopping, etc
        'Retail': ['BEALLS', 'CLOTHING', 'CLOTHES', 'SHOES', 'SHOE', 'UNIQLO', 'GOODWILL',
            'FAMOUSFOOTWEAR', 'GH BASS', 'WAL-MART','TJMAXX','ACADEMY SPORTS','TREASURE COAST CYCLE',
            'NEWEGG','COOLSTUFFINC.COM','JCPENNEY','CHRISTMAS TREE','EBAY','AT HOME STORE',
            'BOYS AND GIRLS CLUB','HABITAT FOR HUMANITY','BIG LOTS','MICHAELS','HOBBY-LOBBY',
            'EBAYINCSHIP','LONDONTRUST','SILVER FEATHER JEWELRY','FIVE BELOW', 'FORTKNOXIIG',
            'CALIFORNIAT','CSTRIMARCO', 'H&M', 'UBIQUITI', 'CANAKIT','PAYPAL INST XFER EBA',
            'PAYPAL INST XFER BLI','PAYPAL INST XFER GIG'],

        # SERVICES
        # any costs related to school
        'Services - Education': ['UCF GRAD','COLLEGE'],
        'Services - Healthcare': ['FL HOSP','UCF STUDENT CENTER','CTR BEHA HEALTH'],
        
        # Payments to attorneys and other professionals for services rendered
        'Services - Professional Services': ['TAGS-DL', 'FL DL & TAG', 'ORLANDO SCHOOL OF MUSI','EXPERIAN', 'ALLIANCE & ASSOC',
            'MYFLORIDACOUNTY', 'MD AIR CONDITIONING A'],
        
        # Computer/Internet transactions for subscriptions or software
        'Services - Computer': ['AZURE', 'ONEDRIVE', 'MICROSOFT', 'MSFT', 'PROTON','NAMECHEAP','NAME-CHEAP',
            'LASTPASS','WWW GRC COM','COMPUTER','XFER  MIC','SPOTIFYUSAI','INST XFER  GIG', '*PNTSTRLAB', 
            'BACKBLAZE', 'EVERNOTE','Tesla','SPOTIFY'],
        'Services - Postage/Mailing': ['USPS','OFFICEMAX','THE SPOT','SHIPPING','FEDEX'],
        'Services - Taxes': ['TURBOTAX', 'FREETAXUSA','TAX PYMT'],

        # TRAVEL
        'Travel - Airlines': [],
        'Travel - Auto/Vehicle Rental': ['UBER'],
        'Travel - Lodging': ['COUNTRY INN & STES','AIR BNB'],
        'Travel - Misc': ['EULONIA COUNTRY STORE', 'FIELD & STREAM','DUNCAN AND YORK','KRESS','SAINT AUGUSTI FL',
            'WARWICK RI','PROVIDENCE RI','HUDSON NEWS'],
        'Travel - Tolls': ['E-PASS','CFX']
    }


    # dictionary of paypal transactions
    pp_transactions = {
        'ebay Shipping': ['EBAY INC SHIPPING'],
        'Donation': ['Donation Payment'],
        'Income': ['eBay Auction Payment'],
        'Online Purchases': ['PreApproved Payment Bill User Payment',
                             'Express Checkout Payment','General Authorization'],
        'Paypal Credit': ['Buyer Credit Payment Withdrawal'],
        'Ignore': ['BML Credit', 'BML Withdrawal', 'General Incentive',
                   'General Payment', 'Reversal of General Account', 'Order',
                   'Account Hold for Open Authorization', 'Bank Deposit to PP',
                   'Void of Authorization', 'Payment Refund']
    }


    # create a parser object
    parser = argparse.ArgumentParser(description='bank statement .csv input, adds a tag column and writes pre defined categories to column based on description of a transaction')

    # expects a system path
    parser.add_argument('fpath', help="Expects a system path to a file.")

    # assigns arguments to parse object
    args = parser.parse_args()

    # sets required argument to filepath variable
    fpath = args.fpath

    # 1. adds a header tag if it doesn't exist
    # 2. checks description columns and adds a category for each transactions based on the dictionaries
    read_csv_add_tags(fpath, transactions, pp_transactions)

    # checks for rows with "None" in the Tag column. 
    # this is done after all transactions are tagged
    check_none_tag(fpath)

    # condenses all csv files into a master file
    create_master_csv(fpath)

    # checks master csv file for duplicate rows, deletes them and sorts them
    check_for_duplicates(fpath)

    # transfers master csv files to budget worksheet
    #master_csv_to_excel_sheet(fpath, checking_cols, credit_cols, paypal_cols)
    

# Driver Code
if __name__ == '__main__':

    # Calling main() function
    main()
