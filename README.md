# tag-bankcsv

A script that opens up a bank statement in .csv format and categorizes each transaction based on a dictionary.

## EXAMPLES
python3 tag-bankcsv.py "/path/to/directory/"
